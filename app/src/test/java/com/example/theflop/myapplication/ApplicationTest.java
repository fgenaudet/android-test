package com.example.theflop.myapplication;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

@Config(manifest = "src/main/AndroidManifest.xml", emulateSdk = 18, reportSdk = 18 )
@RunWith(RobolectricTestRunner.class)
public class ApplicationTest {
   private MainActivity activity;

    @Before
    public void init() {
        Robolectric.setDefaultHttpResponse(200, "[]");
        activity = Robolectric.buildActivity(MainActivity.class)
                .create()
                .start()
                .resume()
                .get();
    }

    @Test
    public void shouldNotFail() {
        assertTrue("Should not fail", true);
    }

    @Test
    public void componentShouldNotBeNull() {
        assertNotNull("Activity should not be null", activity);
        assertNotNull("FrameLayout should not be null", activity.findViewById(R.id.container));
    }

    @Test
    public void fragmentComponentShouldNotNeNull() {
        assertNotNull("TextView should not be null", activity.findViewById(R.id.section_label));
    }
}