package com.example.theflop.myapplication.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.theflop.myapplication.R;
import com.example.theflop.myapplication.beans.Quicky;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TheFlop on 03/02/2015.
 */
public class QuickyAdapter extends ArrayAdapter<Quicky> {
    public QuickyAdapter(Context context, List<Quicky> quickies) {
        super(context, R.layout.item_quicky, quickies);
    }

    // View lookup cache
    private static class ViewHolder {
        TextView title;
        TextView date;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Quicky quicky = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_quicky, parent, false);
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.date = (TextView) convertView.findViewById(R.id.date);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data into the template view using the data object
        viewHolder.title.setText(quicky.getTitle());
        viewHolder.date.setText(quicky.getDate());
        // Return the completed view to render on screen
        return convertView;
    }

}
