package com.example.theflop.myapplication.beans;

/**
 * Created by TheFlop on 03/02/2015.
 */
public class Quicky {
    private String id;
    private String title;
    private String date;

    public Quicky() {
        this.id = String.valueOf(System.currentTimeMillis());
        this.title = "TEST";
        this.date = "24 Nov 2015";
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
